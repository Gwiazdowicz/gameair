﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class ScoreBoard : MonoBehaviour {

    static public int score;
    public GameObject CoinOne;
    public GameObject CoinTwo;
    public GameObject CoinThree;
    CollisionHandler collision;
    Text scoreText ;



    // Use this for initialization
    void  Update() {


        Scene scene = SceneManager.GetActiveScene();
        if (scene.buildIndex == 9 )
        {
            LoseHit(0);
           
        }
        else if (scene.buildIndex == 10){

            LoseHit(0);
        }
        else if (scene.buildIndex == 11)
        {

            LoseHit(0);
        }

        else
        {
            Coin1();
            Coin2();
            Coin3();
            scoreText = GetComponent<Text>();
            scoreText.text = score.ToString();
        }
 
    }

    public void LoseHit(int scoreIn)
    {
        score = score * scoreIn;
        scoreText = GetComponent<Text>();
        scoreText.text = score.ToString();

    }

    void Coin1()
    {
        if (score >= 250)
        {
            CoinOne.SetActive(true);

        }
        else
        {
            CoinOne.SetActive(false);
        }
    }
    void Coin2()
    {
        if (score >= 400)
        {
            CoinTwo.SetActive(true);

        }
        else
        {
            CoinTwo.SetActive(false);
        }
    }
    void Coin3()
    {
        if (score >= 500)
        {
            CoinThree.SetActive(true);

        }
        else
        {
            CoinThree.SetActive(false);
        }
    }
    public void ScoreHit(int scoreIncrease)
    {
        score = score + scoreIncrease;
        scoreText.text = score.ToString();
    }
 
    }


             
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class CollisionHandler : MonoBehaviour
{
    [SerializeField] int scorePerLose = 0;
    [Tooltip("In seconds")] [SerializeField] float levelLoadDelay = 1f;
    [Tooltip("FX prefab on player")] [SerializeField] GameObject deathFX;
    static public int score;
    ScoreBoard scoreBoard;

    void Start()
    {
  
        scoreBoard = FindObjectOfType<ScoreBoard>();
    }

    void OnTriggerEnter(Collider Enemy)
    {
        if (Enemy.gameObject.tag == "Finish")
        {
            WinLevel();
        }
        else if (Enemy.gameObject.tag == "Finish1")
        {
            WinLevel1();
        }
        else if (Enemy.gameObject.tag == "Finish2")
        {
            WinLevel2();
        }
        else if (Enemy.gameObject.tag == "Level2")
        {
            DeathSequence();
            deathFX.SetActive(true);
            Invoke("ReloadScene1", levelLoadDelay);
            LoseHit();


        }

        else if (Enemy.gameObject.tag == "Level1")
        {
            
            DeathSequence();
            deathFX.SetActive(true);
            Invoke("ReloadScene", levelLoadDelay);
            LoseHit();


        }
        else if (Enemy.gameObject.tag == "Level3")
        {

            DeathSequence();
            deathFX.SetActive(true);
            Invoke("ReloadScene2", levelLoadDelay);
            LoseHit();


        }
        else
        {
           
            DeathSequence();
            deathFX.SetActive(true);
            Invoke("ReloadScene", levelLoadDelay);
            LoseHit();


        }
    }

    private void DeathSequence()
    {
       //   SendMessage("OnPlayerDeath");
    }
    private void ReloadScene()
    {
        SceneManager.LoadScene(9);
    }
    private void ReloadScene1()
    {
        SceneManager.LoadScene(11);
    }
    private void ReloadScene2()
    {
        SceneManager.LoadScene(13);
    }
    private void WinLevel()
    {
        SceneManager.LoadScene(7);
    }
    private void WinLevel1()
    {
        SceneManager.LoadScene(8);
    }
    private void WinLevel2()
    {
        SceneManager.LoadScene(12
            );
    }
    public void LoseHit()
    {

    }


}


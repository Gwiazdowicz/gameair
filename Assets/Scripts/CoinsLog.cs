﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsLog : MonoBehaviour {

    static public int score;
    public GameObject CoinOne;
    public GameObject CoinTwo;
    public GameObject CoinThree;
    public void CoinsCount(int score)
    {
        if (score >= 500)
        {
            CoinOne.SetActive(true);
            CoinTwo.SetActive(false);
            CoinThree.SetActive(false);

        }
    }
}

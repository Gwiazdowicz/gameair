﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotor : MonoBehaviour {

    private CharacterController controller;

    private void Start()
    {
        controller = GetComponent<CharacterController>();
    }
    private Vector3 moveVector;

    private void Update()
    {
        Vector3 moveVextor = Vector3.zero;

        moveVector.x = Input.GetAxis("Horizontal");
        moveVector.y = Input.GetAxis("Vertical");

        controller.Move(moveVector * Time.deltaTime);
    }

}

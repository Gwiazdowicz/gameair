﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput; 

public class Player : MonoBehaviour {


  [Header("General")]
  [Tooltip("In ms^-1")]  [SerializeField] float xSpeed = 4f;
  [Tooltip("In m")] [SerializeField] float xRange = 5f;
  [Tooltip("In m")] [SerializeField] float yRange = 10f;
  
  [SerializeField] GameObject FireR;
  [SerializeField] GameObject FireL;


    [Header("Screen-position Based")]
    [SerializeField] float positionPitch = 2f;
    [SerializeField] float controlPitch = -30f;
    [SerializeField] float positionYaw = -2f;
    [SerializeField] float controlRoll = -10f;


    float xThrow, yThrow;
    bool isControlEnabled = true;
    // Use this for initialization
  
     void OnCollisionEnter(Collision collision) {

   
      print("Player hit something");
    }


    // Update is called once per frame
    void Update()
    {
        if (isControlEnabled)
        {
            ProcessTranslation();
            ProcessRotation();
            ProcessFire();
        }
    }
    void OnPlayerDeath()
    {
        isControlEnabled = false;
    }

    private void ProcessTranslation() {

        float xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        float yThrow = CrossPlatformInputManager.GetAxis("Vertical");

        float yOffset = yThrow * xSpeed * Time.deltaTime;
        float xOffset = xThrow * xSpeed * Time.deltaTime ;

        float rawNewXPos = transform.localPosition.x + xOffset;
        float clampedXPos = Mathf.Clamp(rawNewXPos, -xRange, xRange);

        float rawNewYPos = transform.localPosition.y + yOffset;
        float clampedYPos = Mathf.Clamp(rawNewYPos, -yRange, yRange);

        transform.localPosition = new Vector3(clampedXPos, transform.localPosition.y, transform.localPosition.z);
        transform.localPosition = new Vector3(transform.localPosition.x, clampedYPos, transform.localPosition.z);

    }
    private void ProcessRotation()
    {
        float pitchDueToPosition = transform.localPosition.y * positionPitch;
        float pitchDueToControlThrow = yThrow * controlPitch;
        float pitch = pitchDueToPosition + pitchDueToControlThrow;

        float yaw = transform.localPosition.x * positionYaw; 


        float roll = xThrow * controlRoll ;

        transform.localRotation = Quaternion.Euler(pitch,yaw,roll);

    }
   void ProcessFire() {
        if (CrossPlatformInputManager.GetButton("Fire"))
        {
            ActivateGuns();
        }
        else
        {
            DisActiveFire();
        }
    }

    private void DisActiveFire()
    {
        FireR.SetActive(false);
        FireL.SetActive(false);
    }

    void ActivateGuns()
    {
        FireR.SetActive(true);
        FireL.SetActive(true);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FlagSelect: MonoBehaviour
{
    private int index;

    private GameObject[] flagList;

    public void ToggleLeft()
    {
        //Toggle off the current model
        flagList[index].SetActive(false);

        index--;// ind -=1 ; ind  = ind -1;
        if (index < 0)
            index = flagList.Length - 1;

        //Toggle on the new model
        flagList[index].SetActive(true);
    }
    public void ToggleRight()
    {
        //Toggle off the current model
        flagList[index].SetActive(false);

        index--;// ind -=1 ; ind  = ind -1;
        if (index < 0)
            index = flagList.Length - 1;

        //Toggle on the new model
        flagList[index].SetActive(true);
    }
    public void ConfirmButton()
    {
        PlayerPrefs.SetInt("FlagSelected", index);
        SceneManager.LoadScene("Level1");
    }
    void Start()
    {
        index = PlayerPrefs.GetInt("FlagSelected");
        flagList = new GameObject[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
        {
            flagList[i] = transform.GetChild(i).gameObject;
        }
        foreach (GameObject go in flagList)
        {
            go.SetActive(false);
        }
        if (flagList[index])
            flagList[index].SetActive(true);
    }

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Wrog : MonoBehaviour
{

    [SerializeField] GameObject deathEFX;
    [SerializeField] Transform parent;
    [SerializeField] int scorePerHit = 50;
    [SerializeField] int HealthValume = 0;
    ScoreBoard scoreBoard;

    // Use this for initialization
    void Start()
    {
        AddBoxCollider();
        scoreBoard = FindObjectOfType<ScoreBoard>();
    }

    private void AddBoxCollider()
    {
        Collider boxColider = gameObject.AddComponent<BoxCollider>();
        boxColider.isTrigger = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnParticleCollision(GameObject other)
    {

        scoreBoard.ScoreHit(scorePerHit);
        if (HealthValume <= 0)
        {
            KillMethod();
        }

    }

    private void KillMethod()
    {
        GameObject fx = Instantiate(deathEFX, transform.position, Quaternion.identity);
        fx.transform.parent = parent;
        Destroy(gameObject);
    }

}


